import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BettingSystem {

    private static Map<String, Player> players = new HashMap<>();
    private static Map<String, Match> matches = new HashMap<>();

    public static void main(String[] args) {
        BettingSystem bettingSystem = new BettingSystem();

        // Load player data from file
        bettingSystem.loadPlayers("src/main/resources/player_data.txt");

        // Load match data from file
        bettingSystem.loadMatches("src/main/resources/match_data.txt");

        // Process player actions
        bettingSystem.processPlayerActions("src/main/resources/player_data.txt");

        // Write results to file
        bettingSystem.writeResults("src/main/resources/result.txt");
    }

    private void loadPlayers(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                String playerId = parts[0].trim();
                Player player = new Player(playerId);
                players.put(playerId, player);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadMatches(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                String matchId = parts[0].trim();
                double rateA = Double.parseDouble(parts[1].trim());
                double rateB = Double.parseDouble(parts[2].trim());
                Match match = new Match(matchId, rateA, rateB);
                matches.put(matchId, match);
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void processPlayerActions(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                String playerId = parts[0].trim();
                String action = parts[1].trim();
                Player player = players.get(playerId);

                if (player != null) {
                    switch (action) {
                        case "DEPOSIT":
                            int depositAmount = Integer.parseInt(parts[3].trim());
                            player.deposit(depositAmount);
                            break;
                        case "WITHDRAW":
                            int withdrawAmount = Integer.parseInt(parts[3].trim());
                            player.withdraw(withdrawAmount);
                            break;
                            case "BET":
                                try {
                                    String betAmountStr = parts[3];
                                    if (isNumeric(betAmountStr)) {
                                        // Handle bet action using Integer.parseInt(betAmountStr)
                                    } else {
                                        System.out.println("Error: Invalid bet amount format in line - " + line);
                                    }
                                } catch (NumberFormatException e) {
                                    System.out.println("Error: Invalid bet amount format in line - " + line);
                                }
                                break;
                    }
                }
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void writeResults(String filePath) {
        try (FileWriter writer = new FileWriter(filePath)) {
            for (Player player : players.values()) {
                writer.write("Player " + player.getPlayerId() + " balance: " + player.getCoins() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
