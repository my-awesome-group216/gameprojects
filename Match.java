public class Match {
    private String matchId;
    private double rateA;
    private double rateB;

    public Match(String matchId, double rateA, double rateB) {
        this.matchId = matchId;
        this.rateA = rateA;
        this.rateB = rateB;
    }

    public String getMatchId() {
        return matchId;
    }

    public double getRateA() {
        return rateA;
    }

    public double getRateB() {
        return rateB;
    }

    public void processResult(Player player, String result) {
        System.out.println("Match " + matchId + " result: " + result);
        switch (result) {
            case "A":
                player.deposit((int) Math.floor(player.getCoins() * rateA));
                break;
            case "B":
                // Player loses the bet amount
                break;
            case "Draw":
                // Player gets back the bet amount
                player.deposit((int) Math.floor(player.getCoins() * rateA));
                break;
        }
    }
}
