

public class Player {
    private String playerId;
    private long coins;

    public Player(String playerId) {
        this.playerId = playerId;
        this.coins = 0;
    }

    public String getPlayerId() {
        return playerId;
    }

    public long getCoins() {
        return coins;
    }

    public void deposit(int amount) {
        coins += amount;
    }

    public boolean withdraw(int amount) {
        if (amount <= coins) {
            coins -= amount;
            return true;
        } else {
            System.out.println("Error: Insufficient balance for withdrawal.");
            return false;
        }
    }

    public void placeBet(int betAmount, double matchRate, String chosenSide) {
        if (betAmount > coins) {
            System.out.println("Error: Insufficient balance for the bet.");
            return;
        }

        if (chosenSide.equals("A")) {
            coins += (int) Math.floor(betAmount * matchRate);
        } else if (chosenSide.equals("B")) {
            coins -= betAmount;
        }

        // Draw scenario is handled by not changing the coins
    }
}
